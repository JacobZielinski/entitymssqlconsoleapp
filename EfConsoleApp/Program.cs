using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace EfConsoleApp
{
    public class Program
    {
        public static void Main(string[] args)
        {
            
            PerformDatabaseOperations();
            
            Console.ReadLine();
        }


        public static void PerformDatabaseOperations()
        {
            using (var db = new PersonDbContext())
            {
                Console.WriteLine("Add Person to data base...Name<Enter> Surename<Enter>");
                var person = new Person
                {
                    PersonId = 1234,                  
                    FirstName = Console.ReadLine(),
                    LastName = Console.ReadLine(),
                    BirthDate = DateTime.Now
                };
                

                db.Persons.Add(person);
                db.SaveChanges();
                Console.WriteLine("Person added!");
                Console.WriteLine("To display content of the database press 1, to add more People press 2 or anything else to exit");
                var choice = Console.ReadLine();
                if (choice == "1")
                {
                    foreach (var people in db.Persons)
                    {
                        Console.WriteLine("Name: {0}", people.FirstName);
                        Console.WriteLine("Surename: {0}", people.LastName);
                        Console.WriteLine("Person added to database on: {0}", people.BirthDate);

                    }
                }else if (choice == "2")
                {
                    Repeat();
                }
                else
                {
                    Console.WriteLine("Press any key to exit");
                    Console.ReadKey();
                                     
                }
                
          }
        }
        public static void Repeat()
        {
            PerformDatabaseOperations();

        }
    }
}
